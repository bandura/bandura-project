window.onload = function () {

    const menuBTN = document.getElementById('open-menu');
    const menuWrapper = document.getElementsByClassName('menu-wrapper')[0];
    const menuBTNclose = document.getElementById('close-menu');

    menuBTN.onclick = function () {
        menuBTN.style.display='none';
        menuBTNclose.style.display='block';
        menuWrapper.style.display='flex';
    };
    menuBTNclose.onclick = function () {
        menuBTN.style.display='block';
        menuBTNclose.style.display='none';
        menuWrapper.style.display='none';
    };



};