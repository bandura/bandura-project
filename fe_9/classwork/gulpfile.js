'use strict';


const gulp = require('gulp');
const sass = require('gulp-sass');

sass.compiler = require('node-sass');

gulp.task('sass-01', function () {

    return gulp.src('app-01/scss/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest('app-01/css'));
});

gulp.task('sass-02', function () {

    return gulp.src('hw-01/scss/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest('hw-01/css'));
});

gulp.task('watch',function () {

    gulp.watch('app-01/scss/*.scss',gulp.series('sass-01'));

});

gulp.task('watch2',function () {

    gulp.watch('hw-01/scss/*.scss',gulp.series('sass-02'));

});