'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const autoprefix = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const rigger = require('gulp-rigger');
//const del = require('del');
const browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');


const cssArray = [
   './node_modules/normalize.css/normalize.css',  //порядок підключення стилів
    './src/css/**/*.css'
];

const jsArray = [
    './src/js/**/drop-menu.js',                        // js файли які обєднуються
    './src/js/**/show-footer.js',

];

// gulp.task('default', function () {
//     gulp.src('app/*.js')
//         .pipe(rigger())
//         .pipe(gulp.dest('build/'));
// });
gulp.task('buildindex', function() {
    return gulp.src('*.html')
        .pipe(rigger())
        .pipe(gulp.dest('build/html/'));
    // done();
});

function styles() {
    return gulp.src(cssArray)
        .pipe(concat('all.css'))
        .pipe(autoprefix({                 //  browsers: ['>0.1%'], згідно рекомендації в консолі переніс в package.json
            cascade: false
        }))
        .pipe(cleanCss({
            level: 1                              // 0,1,2(hard)
        }))
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream());
}

function scripts() {
    return gulp.src(jsArray)
        .pipe(concat('main.js'))
        .pipe(uglify({
            toplevel: true                      // hard compress js
        }))
        .pipe(gulp.dest('./build/js'))
        .pipe(browserSync.stream());
}


function sassCompil() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/css'))
        .pipe(browserSync.stream());
}

// function clean() {
//    return  del(['build/*']);
// }

function watch() {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        tunnel: true
    });
    gulp.watch('./src/scss/**/*.scss', sassCompil);
    gulp.watch('./src/css/**/*.css', styles);
    gulp.watch('./src/js/**/*.js', scripts);
    gulp.watch('./*html', browserSync.reload);


}

gulp.task('sassCompil', sassCompil);
gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('watch', watch);
// gulp.task('buildindex', buildindex);

gulp.task('build', gulp.parallel(sassCompil, styles, styles, scripts));

// gulp.task('build', gulp.series(clean,
//                    gulp.parallel(sassCompil, styles, styles)
//                     ));
//module.exports = watch;
