$('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 370,
    fitWidth: true,
    gutter: 5,
});

$('.grid').on('click', '.grid-item', function () {
    $(this).toggleClass('bigger');
    $('.grid').masonry('layout');

});
