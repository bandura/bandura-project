$(document).ready(function () {
    $('.slider-single').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        adaptiveHeight: false,
        infinite: true,
        useTransform: true,
        speed: 200,
        asNavFor: '.slider-nav',
        variableWidth: false,
        centerMode: true,
    });
    $('.slider-nav').on('click', function () {
        $('div.is-active').removeClass('is-active');
    })
        .slick({
            arrows: true,
            prevArrow: '<button type="button"  class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
            nextArrow: '<button type="button"  class="slick-next"><i class="fas fa-chevron-right"></button>',
            slidesToShow: 4,
            // slidesToScroll: 1,
            dots: false,
            focusOnSelect: false,
            infinite: true,
            adaptiveHeight: false,
            centerMode: true,
            asNavFor: '.slider-single',
            variableWidth: false,
            initialSlide: 0,
            speed: 200,
            useCSS: true,
        });
    $('.slider-single').on('afterChange', function (event, slick, currentSlide) {
        $('.slider-nav').slick('slickGoTo', currentSlide);
        let currentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
        $('.slider-nav .slick-slide.is-active').removeClass('is-active');
        $(currentNavSlideElem).addClass('is-active');
        switch (currentSlide) {
            case 0:
                $('.what-says').text('Ali says...Integer dignissim, augue tempus ultricies luctus, ' +
                    'quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget ' +
                    'aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio' +
                    ' nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.');
                break;
            case 1:
                $('.what-says').text('Oleg says...Integer dignissim, augue tempus ultricies luctus, ' +
                    'quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget ' +
                    'aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio' +
                    ' nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.');
                break;
            case 2:
                $('.what-says').text('Nata says...Integer dignissim, augue tempus ultricies luctus, ' +
                    'quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget ' +
                    'aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio' +
                    ' nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.');
                break;
            case 3:
                $('.what-says').text('Olena says...Integer dignissim, augue tempus ultricies luctus, ' +
                    'quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget ' +
                    'aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio' +
                    ' nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.');
                break;
            case 4:
                $('.what-says').text('Tedor says...Integer dignissim, augue tempus ultricies luctus, ' +
                    'quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget ' +
                    'aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio' +
                    ' nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.');
                break;
            default:
                break;
        }
    });
    $('.slider-nav').on('click', '.slick-slide', function (event) {
        event.preventDefault();
        let goToSingleSlide = $(this).data('slick-index');
        // console.log(goToSingleSlide);
        $('.slider-single').slick('slickGoTo', goToSingleSlide);
    });
});