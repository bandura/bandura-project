$(document).ready(function () {
    $('.slider-single').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        adaptiveHeight: false,
        infinite: true,
        useTransform: true,
        speed: 1,
        asNavFor: '.slider-nav',
        variableWidth: false,
        centerMode: true,
    });

    $('.slider-nav').on('click', function () {
        $('div.is-active').removeClass('is-active');

    })
        .slick({
            arrows: true,
            prevArrow: '<button type="button"  class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
            nextArrow: '<button type="button"  class="slick-next"><i class="fas fa-chevron-right"></button>',
            slidesToShow: 4,
            // slidesToScroll: 1,
            dots: false,
            focusOnSelect: false,
            infinite: true,
            adaptiveHeight: false,
            centerMode: true,
            asNavFor: '.slider-single',
            variableWidth: false,
            initialSlide: 0,
            speed: 200,

        });

    $('.slider-single').on('afterChange', function (event, slick, currentSlide) {
        $('.slider-nav').slick('slickGoTo', currentSlide);
        let currentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
        $('.slider-nav .slick-slide.is-active').removeClass('is-active');
        $(currentNavSlideElem).addClass('is-active');
        console.log($('.slider-nav'));

        switch (currentSlide) {
            case 0:
                $('.what-says').text('ali says.....................');
                break;
            case 1:
                $('.what-says').text('oleg says.....................');
                break;
            case 2:
                $('.what-says').text('nata says.....................');
                break;
            case 3:
                $('.what-says').text('olena says.....................');
                break;
            case 4:
                $('.what-says').text('tedor says.....................');
                break;
            default:
                break;
        }


    });

    $('.slider-nav').on('click', '.slick-slide', function (event) {
        event.preventDefault();
        let goToSingleSlide = $(this).data('slick-index');
        $('.slider-single').slick('slickGoTo', goToSingleSlide);
    });

});