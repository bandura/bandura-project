// let i = 0;let start = Date.now();
// function count() {
//     for (let j = 0; j < 2e9; j++)
//     {i++;}
//     console.log("Done in " + (Date.now() - start) + 'ms');
// }count();

// for (var i = 1; i < 3; i++) setTimeout(() => { console.log(i) }, 100)

let i = 0;
setTimeout(() => {
    i++;
    const timeoutId = setInterval(() => {
        console.log(i);
        if(i >= 100) clearInterval(timeoutId);
    }, 100);
});