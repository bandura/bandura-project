window.onload = function (){

    let openEyeElement = document.getElementById('openeye');
    let closeEyeElement = document.getElementById('closedeye');
    let openEyeElementSecond = document.getElementById('openeyesecond');
    let closeEyeElementSecond = document.getElementById('closedeyesecond');
    let firstPasswordElement = document.getElementById('firstpassword');
    let secondPasswordElement = document.getElementById('secondpassword');


    closeEyeElement.onmousedown = function () {
        openEyeElement.setAttribute('style', 'display:block');
        closeEyeElement.setAttribute('style', 'display:none');
        firstPasswordElement.setAttribute('type', 'text');
    };

    openEyeElement.onmouseup = function () {
        openEyeElement.setAttribute('style', 'display:none');
        closeEyeElement.setAttribute('style', 'display:block');
        firstPasswordElement.setAttribute('type', 'password');

    };

    closeEyeElementSecond.onmousedown = function () {
        openEyeElementSecond.setAttribute('style', 'display:block');
        closeEyeElementSecond.setAttribute('style', 'display:none');
        secondPasswordElement.setAttribute('type', 'text');

    };

    openEyeElementSecond.onmouseup = function () {
        openEyeElementSecond.setAttribute('style', 'display:none');
        closeEyeElementSecond.setAttribute('style', 'display:block');
        secondPasswordElement.setAttribute('type', 'password');

    };

    document.getElementById('btn').addEventListener('click', comparePasswords);

    function comparePasswords() {

        let passwordValue = document.getElementById('firstpassword').value;
        let comparePasswordsValues = document.getElementById('secondpassword').value;

        if (passwordValue !== comparePasswordsValues) {
            document.getElementById('btn').setAttribute('type', 'button');
            let errorElement = document.getElementById('error');
            errorElement.setAttribute('style', 'display:block');
            errorElement.innerHTML = `Нужно ввести одинаковые значения`;
            firstPasswordElement.onfocus = function () {
                errorElement.innerHTML = '';
            };
            secondPasswordElement.onfocus = function () {
                errorElement.innerHTML = '';
            }

        }

        if ((passwordValue === '') && (comparePasswordsValues === '')) {
            alert('Поле password пустое!')
        }

        if ((passwordValue === comparePasswordsValues) && ((passwordValue !== '') && (comparePasswordsValues !== ''))) {

            document.getElementById('error').setAttribute('style', 'display:none');
            document.getElementById('btn').setAttribute('type', 'button');

            alert('You are welcome))');

        }


    }
};