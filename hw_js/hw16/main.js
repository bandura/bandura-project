
/**
 * @desc Функція пошуку факторіалу числа
 * @param {Number} a
 * @return {Number}
 **/

function factorial(a) {
    while (!isFinite(a))
    {
        a = +prompt('Only numbers!!!');
    }

    if (a===1)
    {
        return 1;
    }
    if (a===0) {
        return Math.pow(factorial(a+1),0);
    }
    else {
        return a*factorial(a-1);
    }



}