$(document).ready(function () {
    $('.hideShowBtn1').click(function () {
        $(this).toggleClass('hideShowBtn2');
        $('.section_wrapper').slideToggle('slow');
    });
    const showHideBtn = $('#top_btn');
    $(window).scroll(function () {
        if ($(window).scrollTop() > 1000) {
            showHideBtn.fadeIn('slow');
        } else {
            showHideBtn.fadeOut('slow');
        }
    });
    showHideBtn.click(function () {
        $('html,body').animate({scrollTop: 0}, 500)
    });

    const page = $('html, body');
    $('.href_navigate a').click(function() {
        page.animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 700);
        return false;
    });
});

