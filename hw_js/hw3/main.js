/**
 * @desc Функція виконання арифметичних операцій
 * @param {Number} a
 * @param {Number} b
 // * @param  {string} operator   //  як правильно описати operator?
 * @return {Number}
 **/


function simpleCalc(a,b,operator) {

    if (operator==="+") {
    return a+b;
    }

    if (operator==="-") {
        return a-b;
    }

    if (operator==="*") {
        return a*b;
    }

    if (operator==="/") {
        return a/b;
    }


}