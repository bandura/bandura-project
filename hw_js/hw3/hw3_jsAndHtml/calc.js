/**
 * @desc Функції простих арефметичних операторів
 // * @param {Number} a
 // * @param {Number} b
 // * @param {Number} result
 // * @return {Number}
 **/


function plus() {

    let a;
    let b;
    let result;
    a = +document.getElementById('number1').value;
    b = +document.getElementById('number2').value;
    result = a+b;

    document.getElementById('sout').innerHTML = result;

}

function minus() {

    let a;
    let b;
    let result;
    a = +document.getElementById('number1').value;
    b = +document.getElementById('number2').value;
    result = a-b;

    document.getElementById('sout').innerHTML = result;

}


function multiply() {

    let a;
    let b;
    let result;
    a = +document.getElementById('number1').value;
    b = +document.getElementById('number2').value;
    result = a*b;

    document.getElementById('sout').innerHTML = result;

}

function division () {

    let a;
    let b;
    let result;
    a = +document.getElementById('number1').value;
    b = +document.getElementById('number2').value;
    result = a/b;

    document.getElementById('sout').innerHTML = result;

}
