$(document).ready(function () {
    $('.tabs li').on('click', function () {
        let dataEvent = $(this).data('button');
        let tabBlock = $('.text-block[data-button="' + dataEvent + '"]');
        let titleBlock = $('.tabs-title[data-button="' + dataEvent + '"]');
        $('.text-block.active').removeClass('active');
        $('.tabs-title.active').removeClass('active');
        tabBlock.toggleClass('active');
        titleBlock.toggleClass('active');

    });
});
