
// fib  (n)  1 2 3 4 5 6 7  8  9  10 ...
//   return  1 1 2 3 5 8 13 21 34 55 ...

// fib  -(n)  -1 -2 -3 -4 -5 -6  -7  -8  -9  -10 ...
//   return    1 -1  2 -3  5 -8 -13 -21  34  -55 ...

/**
 * @desc Функція пошуку числа фібоначі по порядковому номеру
 * @param {Number} n
 * @return {Number}
 **/

function fib(n) {
    console.log('fib: ', n);
    if (n<2)                          // if n === 0 => fib(0)=0; if n===1 => fib(1)=1
        return n;

    else {
        return fib(n-1)+fib(n-2);
    }
}

/**
 * @desc Функція пошуку числа фібоначі по відємному порядковому номеру
 * @param {Number} n
 * @return {Number}
 **/

function minusfib(n) {

    if (n===0)
    {
        return 0;
    }
    else if (n===-1){
        return 1;
    }
    else if (n===-2) {
        return -1;
    }
    else {

        return minusfib(n+2)-minusfib(n+1);                        // n=-2; minusfib(-2+2)-minusfib(-2+1) => f(0)-f(-1)
    }

}

/**
 * @desc Функція пошуку числа фібоначі по порядковому номеру методом циклу
 * @param {Number} n, i, fib1, fib2
 * @return {Number}
 **/
function turboFib(n) {

    let fib1=0, fib2=1, turboFib=1;
    for (let i = 1; i < n; i++ )
    {

        turboFib = fib1 + fib2;
        fib1 = fib2;
        fib2 = turboFib;
        console.log( i);

    }
        return turboFib;
}

/**
 * @desc Функція пошуку числа фібоначі по відємному порядковому номеру методом циклу
 * @param {Number} n, i, fib1, fib2
 * @return {Number}
 **/
function minusTurboFib(n) {

    let fib2=0, fib1=1, minusTurboFib=1;
    for (let i = -1; i > n; i-- )
    {

        minusTurboFib = fib2 - fib1;
        fib2 = fib1;
        fib1 = minusTurboFib;
        console.log( i);

    }
    return minusTurboFib;
}